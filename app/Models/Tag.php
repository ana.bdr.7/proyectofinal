<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Producto;

class Tag extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function productos(){
        return $this->belongsToMany(Producto::class,'productos_tags','id_producto','id_tag');
    }
}
