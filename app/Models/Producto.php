<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Categoria;
use App\Models\Level;
use App\Models\User;
use App\Models\Image;
use App\Models\Tag;
use App\Models\Resena;

class Producto extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }

    public function level(){
        return $this->belongsTo(Level::class);
    }

    public function empresa(){
        return $this->belongsTo(User::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'productos_tags','id_producto','id_tag');
    }

    public function resenas(){
        return $this->hasMany(Resena::class);
    }

    
}
