<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Reserva;
use App\Models\Producto;
use App\Models\User;

class Reseña extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function reserva(){
        return $this->belongsTo(Reserva::class);
    }

    public function producto(){
        return $this->belongsTo(Producto::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
