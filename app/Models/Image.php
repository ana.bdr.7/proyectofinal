<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Producto;

class Image extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function producto(){
        return $this->belongsTo(Producto::class);
    }
}
