<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Resena;
use App\Models\User;

class Reserva extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function resena(){
        return $this->belongsTo(Resena::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function empresa(){
        return $this->belongsTo(User::class);
    }
}
