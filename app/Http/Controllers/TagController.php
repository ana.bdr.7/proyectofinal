<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{
    //mostrar todos los tags
    public function index(){

        $tags = Tag::all();

        return redirect()->json($tags);
    }

    //mostrar los detalles de un tag a través de un id
    public function show($id_tag){

        $tag = Tag::find($id_tag);

        return redirect()->json($tag);
    }

    //crear un nuevo tag
    public function create(Request $request){

        try{
            
            $tag = Tag::create([
                'nombre' => $request->nombre
            ]);

            //return redirect()->route('')->with('mensaje','Tag creado correctamente');
            $success = "Tag creado correctamente";

        }catch(Illuminate\Database\QueryException $ex){   

            //return redirect()->route('')->with('mensaje','Error al crear el Tag');
            $success = "Error al crear el Tag";
        }

        return response()->json($success);

    }

    //editar un tag
    public function edit(Request $request){

        try{
            
            $tag = Tag::find($request->id);
            $tag->nombre = $request->nombre;
            $tag->save();
            
            $success = "Tag modificado correctamente";
            
            //return redirect()->route('')->with('mensaje','Tag modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar el Tag";
            //return redirect()->route('')->with('mensaje','Error al modificar el Tag');
        }

        return response()->json($success);

    }

    //borrar un tag
    public function destroy($id_tag){

        try{
            
            $tag = Tag::find($id_tag);
            $tag->delete();

            $success = "Tag eliminado correctamente";

            //return redirect()->route('')->with('mensaje','Tag eliminado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Fallo al eliminar el Tag";

            //return redirect()->route('')->with('mensaje','Fallo al eliminar el Tag');
        }

        return response()->json($success);

    }
}
