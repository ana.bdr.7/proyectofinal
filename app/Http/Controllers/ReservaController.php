<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reserva;

class ReservaController extends Controller
{
    //mostrar todas las reservas
    public function index(){

        $reservas = Reserva::all();

        return redirect()->json($reservas);
    }

    //mostrar los detalles de una reservas a través de un id
    public function show($id_reserva){

        $reserva = Reserva::find($id_reserva);

        return redirect()->json($reserva);
    }

    //mostrar las reservas de un usuario
    public function reservasByUser($user){

        $reservas = DB::table('reservas')
            ->where('id_usuario','=',$user)
            ->get();
        
        return response()->json($reservas);
    }

    //mostrar las reservas de un producto
    public function reservasByProducto($producto){

        $reservas = DB::table('reservas')
            ->where('id_producto','=',$producto)
            ->get();
        
        return response()->json($reservas);
    }

    //crear un nueva reserva
    public function create(Request $request){

        try{
            
            $reserva = Reserva::create([
                'id_producto' => $request->id_producto,
                'id_usuario' => $request->id_usuario,
                'id_empresa' => $request->id_empresa,
                'realizado' => $request->realizado

            ]);

            //return redirect()->route('')->with('mensaje','Reserva creado correctamente');
            $success = "Reserva creado correctamente";

        }catch(Illuminate\Database\QueryException $ex){   

            //return redirect()->route('')->with('mensaje','Error al crear el Reserva');
            $success = "Error al crear el Reserva";
        }

        return response()->json($success);

    }

    //editar una reserva
    public function edit(Request $request){

        try{
            
            $reserva = Reserva::find($request->id);
            $reserva->id_producto = $request->id_producto;
            $reserva->id_usuario = $request->id_usuario;
            $reserva->id_empresa = $request->id_empresa;
            $reserva->realizado = $request->realizado;
            $reserva->save();
            
            $success = "Reserva modificado correctamente";
            
            //return redirect()->route('')->with('mensaje','Reserva modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar el Reserva";
            //return redirect()->route('')->with('mensaje','Error al modificar el Reserva');
        }

        return response()->json($success);

    }

    //borrar una reserva
    public function destroy($id_reserva){

        try{
            
            $reserva = Reserva::find($id_reserva);
            $reserva->delete();

            $success = "Reserva  eliminado correctamente";

            //return redirect()->route('')->with('mensaje','Reserva  eliminado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Fallo al eliminar el Reserva ";

            //return redirect()->route('')->with('mensaje','Fallo al eliminar el Reserva ');
        }

        return response()->json($success);


    }
}
