<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;

class ProductoController extends Controller
{
     //mostrar todos los productos
     public function index(){

        $productos = Producto::all();

        return redirect()->json($productos);
    }

    //mostrar los detalles de un producto a través de un id
    public function show($id_producto){

        $producto = Producto::find($id_producto);

        return redirect()->json($producto);
    }

    //mostrar los productos de un usuario
    public function productosByUser($usuario){

        $productos = Producto::where('id_empresa',$usuario)->get();
        
        return response()->json($productos);
    }

    //crear un nuevo producto
    public function create(Request $request){

        try{
            
            $producto = Producto::create([
                'titulo' => $request->titulo,
                'descripcion' => $request->descripcion,
                'precio' => $request->precio,
                'aforo' => $request->aforo,
                'fecha' => $request->fecha,
                'ubicacion' => $request->ubicacion,
                'id_level' => $request->id_level,
                'id_categoria' => $request->id_categoria,
                'id_empresa' => $request->id_empresa
            ]);

            //return redirect()->route('')->with('mensaje','Producto creado correctamente');
            $success = "Producto creado correctamente";

        }catch(Illuminate\Database\QueryException $ex){   

            //return redirect()->route('')->with('mensaje','Error al crear el Producto');
            $success = "Error al crear el Producto";
        }

        return response()->json($success);

    }

    //editar un producto
    public function edit(Request $request){

        try{
            
            $producto = Producto::find($request->id);
            $producto->titulo = $request->titulo;
            $producto->descripcion = $request->descripcion;
            $producto->precio = $request->precio;
            $producto->aforo = $request->aforo;
            $producto->fecha = $request->fecha;
            $producto->ubicacion = $request->ubicacion;
            $producto->id_level = $request->id_level;
            $producto->id_categoria = $request->id_categoria;
            $producto->id_empresa = $request->id_empresa;
            $producto->save();
            
            $success = "Producto modificado correctamente";
            
            //return redirect()->route('')->with('mensaje','Producto modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar el Producto";
            //return redirect()->route('')->with('mensaje','Error al modificar el Producto');
        }

        return response()->json($success);

    }

    //borrar un producto
    public function destroy($id_producto){

        try{
            
            $producto = Producto::find($id_producto);
            $producto->delete();

            $success = "Producto eliminado correctamente";

            //return redirect()->route('')->with('mensaje','Producto eliminado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Fallo al eliminar el Producto";

            //return redirect()->route('')->with('mensaje','Fallo al eliminar el Producto');
        }

        return response()->json($success);


    }
}
