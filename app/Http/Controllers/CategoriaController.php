<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriaController extends Controller
{   
    //mostrar todas las categorias
    public function index(){

        $categorias = Categoria::all();

        return redirect()->json($categorias);
    }

    //mostrar los detalles de un nivel a través de un id
    public function show($id_categoria){

        $categoria = Categoria::find($id_categoria);

        return redirect()->json($categoria);
    }

    //crear una nueva categoria
    public function create(Request $request){

        try{
            
            $categoria = Categoria::create([
                'tipo' => $request->tipo
            ]);

            $success = "Categoría creada correctamente";

            //return redirect()->route('')->with('mensaje','Categoría creada correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al crear la Categoría";

            //return redirect()->route('')->with('mensaje','Error al crear la Categoría');
        }

        return response()->json($success);

    }

    //editar una categoria
    public function edit(Request $request){

        try{
            
            $categoria = Categoria::find($request->id);
            $categoria->tipo = $request->tipo;
            $categoria->save();
            
            $success = "Categoría modificada correctamente";
            //return redirect()->route('')->with('mensaje','Categoría modificada correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar la Categoría";
            //return redirect()->route('')->with('mensaje','Error al modificar la Categoría');
        }

        return response()->json($success);

    }

    //borrar una categoria
    public function destroy($categoria){

        try{
            
            $categoria = Categoria::find($categoria);
            $categoria->delete();

            $success = "Categoria eliminada correctamente";
            //return redirect()->route('')->with('mensaje','Categoria eliminada correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success ="Fallo al eliminar la categoria";
            //return redirect()->route('')->with('mensaje','Fallo al eliminar la categoria');
        }

        return response()->json($success);

    }
}
