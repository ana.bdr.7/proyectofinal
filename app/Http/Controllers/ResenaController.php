<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resena;

class ResenaController extends Controller
{
    //mostrar todas las reseñas
    public function index(){

        $resenas = Resena::all();

        return redirect()->json($resenas);
    }

    //mostrar los detalles de una reseña a través de un id
    public function show($id_resena){

        $resena = Resena::find($id_resena);

        return redirect()->json($resena);
    }

    //mostrar las reseñas de un usuario
    public function resenasByUser($user){
        $resenas = DB::table('resenas')
            ->where('id_user','=',$user)
            ->get();
        
        return response()->json($resenas);
    }

    //mostrar las resenas de un producto
    public function resenasBýProducto($producto){
        $resenas = DB::table('resenas')
            ->where('id_producto','=',$producto)
            ->get();
        
        return response()->json($resenas);
    }

    //crear un nueva resena
    public function create(Request $request){

        try{
            
            $resena = Resena::create([
                'id_producto' => $request->id_producto,
                'id_user' => $request->id_user,
                'valoracion' => $request->valoracion,
                'calificacion' => $request->calificacion,
                'id_reserva' => $request->id_reserva

            ]);

            //return redirect()->route('')->with('mensaje','Reseña creado correctamente');
            $success = "Reseña creado correctamente";

        }catch(Illuminate\Database\QueryException $ex){   

            //return redirect()->route('')->with('mensaje','Error al crear el Reseña');
            $success = "Error al crear el Reseña";
        }

        return response()->json($success);

    }

    //editar una reseña
    public function edit(Request $request){

        try{
            
            $resena = Resena::find($request->id);
            $resena->id_producto = $request->id_producto;
            $resena->id_user = $request->id_user;
            $resena->valoracion = $request->valoracion;
            $resena->calificacion = $request->calificacion;
            $resena->id_reserva = $request->id_reserva;
            $resena->save();
            
            $success = "Reseña modificado correctamente";
            
            //return redirect()->route('')->with('mensaje','Reseña modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar el Reseña";
            //return redirect()->route('')->with('mensaje','Error al modificar el Reseña');
        }

        return response()->json($success);

    }

    //borrar una resena
    public function destroy($id_resena){

        try{
            
            $resena = Resena::find($id_resena);
            $resena->delete();

            $success = "Reseña eliminado correctamente";

            //return redirect()->route('')->with('mensaje','Reseña eliminado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Fallo al eliminar el Reseña";

            //return redirect()->route('')->with('mensaje','Fallo al eliminar el Reseña');
        }

        return response()->json($success);


    }
}
