<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Level;

class LevelController extends Controller
{
    //mostrar todos los niveles
    public function index(){

        $niveles = Level::all();

        return redirect()->json($niveles);
    }

    //mostrar los detalles de una categoria a través de un id
    public function show($id_nivel){

        $nivel = Level::find($id_nivel);

        return redirect()->json($nivel);
    }

    //crear un nuevo nivel
    public function create(Request $request){

        try{
            
            $nivel = Level::create([
                'dificultad' => $request->dificultad
            ]);

            //return redirect()->route('')->with('mensaje','Nivel creado correctamente');
            $success = "Nivel creado correctamente";

        }catch(Illuminate\Database\QueryException $ex){   

            //return redirect()->route('')->with('mensaje','Error al crear el Nivel');
            $success = "Error al crear el Nivel";
        }

        return response()->json($success);

    }

    //editar un nivel
    public function edit(Request $request){

        try{
            
            $nivel = Level::find($request->id);
            $nivel->dificultad = $request->dificultad;
            $nivel->save();
            
            $success = "Nivel modificado correctamente";
            
            //return redirect()->route('')->with('mensaje','Nivel modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar el Nivel";
            //return redirect()->route('')->with('mensaje','Error al modificar el Nivel');
        }

        return response()->json($success);

    }

    //borrar un nivel
    public function destroy($id_level){

        try{
            
            $nivel = Level::find($id_level);
            $nivel->delete();

            $success = "Nivel eliminado correctamente";

            //return redirect()->route('')->with('mensaje','Nivel eliminado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Fallo al eliminar el Nivel";

            //return redirect()->route('')->with('mensaje','Fallo al eliminar el Nivel');
        }

        return response()->json($success);


    }
}
