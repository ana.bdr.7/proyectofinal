<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;

class RolController extends Controller
{
    //mostrar todos los rols
    public function index(){

        $roles = Rol::all();

        return redirect()->json($roles);
    }

    //mostrar los detalles de un rol a través de un id
    public function show($id_rol){

        $rol = Rol::find($id_rol);

        return redirect()->json($rol);
    }

    //crear un nuevo rol
    public function create(Request $request){

        try{
            
            $rol = Rol::create([
                'tipo' => $request->tipo
            ]);

            //return redirect()->route('')->with('mensaje','Rol creado correctamente');
            $success = "Rol creado correctamente";

        }catch(Illuminate\Database\QueryException $ex){   

            //return redirect()->route('')->with('mensaje','Error al crear el Rol');
            $success = "Error al crear el Rol";
        }

        return response()->json($success);

    }

    //editar un rol
    public function edit(Request $request){

        try{
            
            $rol = Rol::find($request->id);
            $rol->tipo = $request->tipo;
            $rol->save();
            
            $success = "Rol modificado correctamente";
            
            //return redirect()->route('')->with('mensaje','Rol modificado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Error al modificar el Rol";
            //return redirect()->route('')->with('mensaje','Error al modificar el Rol');
        }

        return response()->json($success);

    }

    //borrar un rol
    public function destroy($id_rol){

        try{
            
            $rol = Rol::find($id_rol);
            $rol->delete();

            $success = "Rol eliminado correctamente";

            //return redirect()->route('')->with('mensaje','Rol eliminado correctamente');

        }catch(Illuminate\Database\QueryException $ex){   

            $success = "Fallo al eliminar el Rol";

            //return redirect()->route('')->with('mensaje','Fallo al eliminar el Rol');
        }

        return response()->json($success);

    }
}
