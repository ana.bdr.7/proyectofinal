<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $tags = array(
        array(
            'nombre' => 'EsquíDefondo'
        ),
        array(
            'nombre' => 'Senderismo'
        ),
        array(
            'nombre' => 'Montañismo'
        ),
        array(
            'nombre' => 'MountainBike'
        ),
        array(
            'nombre' => 'Ciclismo'
        ),
        array(
            'nombre' => 'Running'
        ),
        array(
            'nombre' => 'Alpinismo'
        ),
        array(
            'nombre' => 'TrailRunning'
        ),
        array(
            'nombre' => 'EsquíDeMontaña'
        ),
        array(
            'nombre' => 'Kanoa'
        ),
        array(
            'nombre' => 'Raquetas de Nieve'
        ),
        array(
            'nombre' => 'Velero'
        ),
        array(
            'nombre' => 'PaseoACaballo'
        ),
        array(
            'nombre' => 'AlaDelta'
        ),
        array(
            'nombre' => 'Submarinismo'
        ),
        array(
            'nombre' => 'Trineo'
        ),
        array(
            'nombre' => 'EscaladaEnHielo'
        ),
        array(
            'nombre' => 'Kitesurf'
        ),
        array(
            'nombre' => 'ViaFerrata'
        ),
        array(
            'nombre' => 'PaddleSurf'
        ),
        array(
            'nombre' => 'EsquíAlpino'
        ),
        array(
            'nombre' => 'Escalada'
        ),
        array(
            'nombre' => 'Espeleología'
        ),
        array(
            'nombre' => 'Parapente'
        ),
        array(
            'nombre' => 'Snowboard'
        ),
        array(
            'nombre' => 'Barranquismo'
        ),
        array(
            'nombre' => 'KyteSki'
        ),
        array(
            'nombre' => 'Rafting'
        ),
        array(
            'nombre' => 'Deporte'
        ),
        array(
            'nombre' => 'Salud'
        ),
        array(
            'nombre' => 'Nieve'
        ),
        array(
            'nombre' => 'Sport'
        ),
        array(
            'nombre' => 'VidaSana'
        ),
        array(
            'nombre' => 'VidaSaludable'
        ),
        array(
            'nombre' => 'Fuerza'
        ),
        array(
            'nombre' => 'Superacion'
        ),
        array(
            'nombre' => 'DeporteYSalud'
        ),
        array(
            'nombre' => 'DeporteEsVida'
        ),
        array(
            'nombre' => 'DeporteAlAireLibre'
        ),
        array(
            'nombre' => 'DeporteAcuático'
        ),
        array(
            'nombre' => 'DeporteDeAventura'
        )
        

    );
    public function run()
    {
        foreach($this->tags as $tag){
            $a = new Tag();
            $a->nombre = $tag['nombre'];
            $a->save();
        }
        $this->command->info("Tabla tags inicializada correctamente");
    }
}
