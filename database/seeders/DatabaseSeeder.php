<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\CategoriaSeeder;
use Database\Seeders\LevelSeeder;
use Database\Seeders\RolSeeder;
use Database\Seeders\TagSeeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Seeder Categorias
        DB::table('categorias')->delete();
        $this->call(CategoriaSeeder::class);
        //Seeder Levels
        DB::table('levels')->delete();
        $this->call(LevelSeeder::class);
        //Seeder Rols
        DB::table('rols')->delete();
        $this->call(RolSeeder::class);
        //Seeder Tags
        DB::table('tags')->delete();
        $this->call(TagSeeder::class);


        
    }
}
