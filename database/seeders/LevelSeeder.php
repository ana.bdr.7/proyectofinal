<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Level;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $niveles = array(
        array(
            'dificultad' => 'Principiante',
        ),
        array(
            'dificultad' => 'Intermedio',
        ),
        array(
            'dificultad' => 'Avanzado',
        ),
        array(
            'dificultad' => 'Profesional',
        ),
    );
    public function run()
    {
        foreach($this->niveles as $nivel){
            $a = new Level();
            $a->dificultad = $nivel['dificultad'];
            $a->save();
        }

        $this->command->info('tabla levels inicializada correctamente');
    }
}
