<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rol;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $roles = array(
        array(
            'tipo' => 'Cliente',
        ),
        array(
            'tipo' => 'Profesional',
        )
        );
        
        
    
    public function run()
    {
        foreach($this->roles as $rol){
            $a = new Rol();
            $a->tipo = $rol['tipo'];
            $a->save();
        }

        $this->command->info('tabla roles inicializada correctamente');
    }
}
