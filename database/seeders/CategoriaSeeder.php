<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $categorias = array(
        array(
            'tipo' => 'Esquí de fondo'
        ),
        array(
            'tipo' => 'Senderismo'
        ),
        array(
            'tipo' => 'Montañismo'
        ),
        array(
            'tipo' => 'Mountain Bike'
        ),
        array(
            'tipo' => 'Ciclismo'
        ),
        array(
            'tipo' => 'Running'
        ),
        array(
            'tipo' => 'Alpinismo'
        ),
        array(
            'tipo' => 'Trail Running'
        ),
        array(
            'tipo' => 'Esquí de Montaña'
        ),
        array(
            'tipo' => 'Kanoa'
        ),
        array(
            'tipo' => 'Raquetas de Nieve'
        ),
        array(
            'tipo' => 'Velero'
        ),
        array(
            'tipo' => 'Paseo a Caballo'
        ),
        array(
            'tipo' => 'Ala Delta'
        ),
        array(
            'tipo' => 'Submarinismo'
        ),
        array(
            'tipo' => 'Trineo'
        ),
        array(
            'tipo' => 'Escalada en Hielo'
        ),
        array(
            'tipo' => 'Kitesurf'
        ),
        array(
            'tipo' => 'Via Ferrata'
        ),
        array(
            'tipo' => 'Paddle Surf'
        ),
        array(
            'tipo' => 'Esquí Alpino'
        ),
        array(
            'tipo' => 'Escalada'
        ),
        array(
            'tipo' => 'Espeleología'
        ),
        array(
            'tipo' => 'Parapente'
        ),
        array(
            'tipo' => 'Snowboard'
        ),
        array(
            'tipo' => 'Barranquismo'
        ),
        array(
            'tipo' => 'Kyte Ski'
        ),
        array(
            'tipo' => 'Rafting'
        ),
        
        

    );
        
     

    
    public function run()
    {
        foreach($this->categorias as $categoria){
            $a = new Categoria();            
            $a->tipo = $categoria['tipo'];
            $a->save();
        }
        $this->command->info('tabla categorias inicializada correctamente');
    }
}
